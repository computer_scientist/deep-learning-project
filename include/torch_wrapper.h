//
// Created by rasinnovation on 5/21/24.
//

#ifndef C_DEEP_LEARNING_TORCH_WRAPPER_H
#define C_DEEP_LEARNING_TORCH_WRAPPER_H

#ifdef __cplusplus
extern "C" {
#endif

    double compute_binary_cross_entropy(const double*, const double*);

#ifdef __cplusplus
}
#endif

#endif //C_DEEP_LEARNING_TORCH_WRAPPER_H
