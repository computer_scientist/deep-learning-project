#ifndef TRANSPOSE_H
#define TRANSPOSE_H

#include "vector.h"

// Function that calculates softmax and populates the exponential and sigma_vector
void softmax(const Vector*, Vector*, Vector*);
double entropy(const Vector*);
double cross_entropy(const Vector*, const Vector*);
double derivative(double scalar, double variable, double exponent);
void linespace(double* array, double start, double end, int num);

#endif //TRANSPOSE_H
