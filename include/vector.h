#ifndef C_DEEP_LEARNING_VECTOR_H
#define C_DEEP_LEARNING_VECTOR_H

#include <stdlib.h>

typedef struct {
    size_t length;
    double* data;
} Vector;

Vector* vector_create_malloc(size_t);
void vector_free(Vector*);
void vector_initialize(Vector*, const double*);
void vector_initialize_random(Vector*, int, int);
double vector_dot(const Vector*, const Vector*);
void vector_print(const Vector*);
void vector_set(Vector*, size_t, double);
double vector_get(const Vector*, size_t);
double vector_min(const Vector*);
size_t vector_argmin(const Vector*);
double vector_max(const Vector*);
size_t vector_argmax(const Vector*);
double vector_sum(const Vector*);
double vector_mean(const Vector*);
double vector_variance(const Vector*);


#endif //C_DEEP_LEARNING_VECTOR_H
