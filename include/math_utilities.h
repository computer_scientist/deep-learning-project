//
// Created by potro on 5/25/24.
//

#ifndef MATH_UTILITIES_H
#define MATH_UTILITIES_H

// Argmin Argmax Utilities
void demo_argmin_argmax();
void demo_vector_argmin_argmax();
void demo_matrix_argmin_argmax();

// Entropy Utilities
void demo_entropy();
void demo_cross_entropy();
void demo_torch_cross_entropy();

// Log Utility
void demo_logarithm();

// Matrix Utility
void demo_matrix_multiplication();

// Softmax Utility
void demo_softmax();

// Transpose Utility
void demo_transpose_vector();
void demo_transpose_matrix();

// Vector Dot Product Utility
void demo_vector_dot_product();

// Mean Variance Utility
void demo_mean_variance();

#endif //MATH_UTILITIES_H
