#ifndef C_DEEP_LEARNING_MATRIX_H
#define C_DEEP_LEARNING_MATRIX_H

#include <stdlib.h>
#include <stdio.h>


// Define the Matrix structure
typedef struct {
    size_t rows;
    size_t columns;
    double* data;
} Matrix;

// Create a matrix struct by allocating and setting attributes
Matrix* matrix_create_malloc(size_t, size_t);

// Free memory that was allocated for the matrix
void matrix_free(Matrix*);

// Initializes matrix with values from passed in array
void matrix_initialize(Matrix*, const double*);

// Initializes matrix with random values from 0 to 3
void matrix_random_initialize(Matrix*);

// Function to print a matrix
void matrix_print(const Matrix*);

// Function that sets value at a particular row column index
void matrix_set(const Matrix* matrix, size_t, size_t, double);

// Function that returns value at a particular row column index
double matrix_get(const Matrix*, size_t, size_t);

// Function to transpose a matrix
void matrix_transpose(const Matrix*, Matrix*);

// Fills a result matrix with values from multiplying to matrices
void matrix_multiplication(const Matrix*, const Matrix*, Matrix*);

size_t* matrix_argmin(const Matrix*, size_t*);

size_t* matrix_argmax(const Matrix*, size_t*);
#endif // C_DEEP_LEARNING_MATRIX_H
