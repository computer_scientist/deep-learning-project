#ifndef C_DEEP_LEARNING_PLOT_H
#define C_DEEP_LEARNING_PLOT_H

#include <stdio.h>
#include <vector.h>

void plot(const Vector*, const Vector*, const char*, const char*, const char*);

#endif //C_DEEP_LEARNING_PLOT_H
