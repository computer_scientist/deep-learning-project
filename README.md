# Deep Learning Course

## Description
This project came from the course "A deep understanding of deep learning (with Python intro) by Mike X Cohen. It contains both python and julia versions.

## Code reuse disclosure
Note: Python versions of the project is free to use but you will need to reference Mike X Cohen and/or contact him for use for commercial purposes as per his request. The julia versions are free to use and referencing me would be appreciated.

## Tracking progress
This project is ongoing at the moment and is part of my enthusiasm in continuous learning. Progress can be tracked on the dev branch like all other projects in my git repository. When a section is completed, I usually do a pull/merge request and such details can be viewed on the master branch.