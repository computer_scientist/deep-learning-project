#include "plot.h"

void plot(const Vector* const x_vector, const Vector* const y_vector,
          const char* const xlabel, const char* const ylabel, const char* const title) {
    FILE* gnuplot = popen("gnuplot -persistent", "w");

    if (gnuplot == NULL) {
        fprintf(stderr, "Error opening pip to GNUplot.\n");
        return;
    }

    fprintf(gnuplot, "set terminal qt font 'Arial,12'\n");

    fprintf(gnuplot, "set xlabel '%s'\n", xlabel);
    fprintf(gnuplot, "set ylabel '%s'\n", ylabel);
    fprintf(gnuplot, "set title '%s'\n", title);
    fprintf(gnuplot, "set xrange [%f:%f]\n",
            vector_min(x_vector) - 1, vector_max(x_vector) + 1);
    fprintf(gnuplot, "set yrange [0:%f]\n", vector_max(y_vector) + 1);
    fprintf(gnuplot, "set key box\n");
    fprintf(gnuplot, "plot '-' with points pt 7 title '%s'\n", title);

    for (size_t i = 0; i < x_vector->length; ++i) {
        fprintf(gnuplot, "%g %g\n", vector_get(x_vector, i), vector_get(y_vector, i));
    }

    fprintf(gnuplot, "e\n");
    fflush(gnuplot);

    pclose(gnuplot);
}
