#include "math_utilities.h"
#include "gradient_descent_utilities.h"
#include "stdio.h"

#include <stdlib.h>
#include <time.h>

void gradient_descent() {
    demo_gradient_descent_1d();
}

void math() {
    // demo_transpose_vector();
    // demo_transpose_matrix();
    // demo_vector_dot_product();
    // demo_matrix_multiplication();
//     demo_softmax(); // TODO
//     demo_logarithm(); // TODO
    // demo_entropy();
    // demo_cross_entropy();
    // demo_torch_cross_entropy();
//    demo_vector_argmin_argmax();
//    demo_matrix_argmin_argmax();
//    demo_argmin_argmax();
    // demo_vector_argmin_argmax();
    // demo_matrix_argmin_argmax();
//    demo_argmin_argmax();
//    demo_mean_variance();
}

int main() {
    srand((unsigned int) time(NULL));

//    math();
    gradient_descent();

    return 0;
}
