#include "matrix.h"

#include "float.h"

// Function implementations
Matrix* matrix_create_malloc(const size_t rows, const size_t columns) {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Matrix* matrix = (Matrix*) malloc(sizeof(Matrix));
    if (matrix == NULL) {
        fprintf(stderr, "Failed to allocate memory for matrix structure\n");
        exit(EXIT_FAILURE);
    }
    matrix->rows = rows;
    matrix->columns = columns;
    matrix->data = (double*)calloc(rows * columns, sizeof(double));
    if (matrix->data == NULL) {
        free(matrix);
        fprintf(stderr, "Failed to allocate memory for matrix data\n");
        exit(EXIT_FAILURE);
    }

    return matrix;
}

void matrix_free(Matrix* matrix) {
    //=====================
    // FREE RESOURCE
    //=====================
    free(matrix->data);
    free(matrix);
}

void matrix_print(const Matrix* matrix) {
    for (size_t row_index = 0; row_index < matrix->rows; ++row_index) {
        printf("| ");
        for (size_t column_index = 0; column_index < matrix->columns; ++column_index) {
            printf("%g ", matrix_get(matrix, row_index, column_index));
        }
        printf("|\n");
    }
    printf("\n");
}

void matrix_initialize(Matrix* self, const double* const array) {
    for (size_t row_index = 0; row_index < self->rows; ++row_index) {
        for (size_t column_index = 0; column_index < self->columns; ++column_index) {
            matrix_set(self, row_index, column_index,
                       array[row_index * self->columns + column_index]);
        }
    }
}

void matrix_random_initialize(Matrix* matrix) {
    for (size_t row_index = 0; row_index < matrix->rows; ++row_index) {
        for (size_t column_index = 0; column_index < matrix->columns; ++column_index) {
            matrix_set(matrix, row_index, column_index, rand() % 4);
        }
    }
}

void matrix_set(const Matrix* matrix, const size_t row, const size_t column, const double value) {
    matrix->data[row * matrix->columns + column] = value;
}

double matrix_get(const Matrix* const matrix, const size_t row, const size_t col) {
    return matrix->data[row * matrix->columns + col];
}

void matrix_transpose(const Matrix* src, Matrix* dest) {
    for (size_t row_index = 0; row_index < src->rows; row_index++) {
        for (size_t column_index = 0; column_index < src->columns; column_index++) {
            const double value = matrix_get(src, row_index, column_index);
            matrix_set(dest, column_index, row_index, value);
        }
    }
}

void matrix_multiplication(const Matrix* matrix1, const Matrix* matrix2, Matrix* result_matrix) {
    if (matrix1->columns != matrix2->rows) {
        fprintf(stderr, "Matrix dimensions do not match for multiplication.\n");
        exit(EXIT_FAILURE);
    };

    for (size_t i = 0; i < matrix1->rows; ++i) {
        for (size_t j = 0; j < matrix2->columns; ++j) {
            for (size_t k = 0; k < matrix1->columns; ++k) {
                result_matrix->data[i * result_matrix->columns + j]
                    += matrix1->data[i * matrix1->columns + k]
                            * matrix2->data[k * matrix2->columns + j];
            }
        }
    }
}

size_t* matrix_argmin(const Matrix* const matrix, size_t* min_index) {
    double min = DBL_MAX;

    for (size_t rows = 0; rows < matrix->rows; ++rows) {
        for (size_t columns = 0;  columns < matrix->columns; ++columns) {
            const double temp = matrix_get(matrix, rows, columns);

            if (temp < min) {
                min = temp;
                min_index[0] = rows;
                min_index[1] = columns;
            }
        }
    }

    return min_index;
}

size_t* matrix_argmax(const Matrix* const matrix, size_t* max_index) {
    double max = -DBL_MIN;

    for (size_t rows = 0; rows < matrix->rows; ++rows) {
        for (size_t columns = 0;  columns < matrix->columns; ++columns) {
            const double temp = matrix_get(matrix, rows, columns);

            if (temp > max) {
                max = temp;
                max_index[0] = rows;
                max_index[1] = columns;
            }
        }
    }

    return max_index;
}
