#include "vector.h"
#include <float.h>
#include <math.h>
#include <stdio.h>

Vector* vector_create_malloc(const size_t length) {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* vector = (Vector*) malloc(sizeof (Vector));

    if (vector == NULL) {
        fprintf(stderr, "Failed to allocate memory for vector structure\n");
        exit(EXIT_FAILURE);
    }

    vector->length = length;

    //=====================
    // RESOURCE ALLOCATION
    //=====================
    vector->data = (double*) calloc(length, sizeof(double));

    if (vector->data == NULL) {
        free(vector);
        fprintf(stderr, "Failed to allocate memory for matrix data\n");
        exit(EXIT_FAILURE);
    }

    return vector;
}

void vector_free(Vector* vector) {
    //=====================
    // FREE RESOURCE
    //=====================
    free(vector->data);
    free(vector);
}

void vector_print(const Vector* const vector) {
    printf("| ");
    for (size_t i = 0; i < vector->length; ++i) {
        printf("%g ", vector_get(vector, i));
    }
    printf("|\n\n");
}

void vector_initialize(Vector* vector, const double* const array) {
    for (size_t index = 0; index < vector->length; ++index) {
        vector_set(vector, index, array[index]);
    }
}

void vector_initialize_random(Vector* vector, const int low, const int high) {

    for (size_t index = 0; index < vector->length; ++index) {
        vector_set(vector, index, (rand() % (high - low + 1)) + low);
    }
}

void vector_set(Vector* vector, const size_t index, const double value) {
    vector->data[index] = value;
}

double vector_get(const Vector* const vector, const size_t index) {
    return vector->data[index];
}

double vector_dot(const Vector* const vector1, const Vector* const vector2) {
    double result = 0.0;

    for (size_t index = 0; index < vector1->length; ++index) {
        result += (vector1->data[index] * vector2->data[index]);
    }

    return result;
}

double vector_min(const Vector* const vector) {
    double min = DBL_MAX;

    for (size_t index = 0; index < vector->length; ++index) {
        const double temp = vector_get(vector, index);
        if (temp < min) {
            min = temp;
        }
    }

    return min;
}

size_t vector_argmin(const Vector* const vector) {
    size_t arg_min_index = 0;
    double min = DBL_MAX;

    for (size_t index = 0; index < vector->length; ++index) {

        const double temp = vector_get(vector, index);
        if (temp < min) {
            min = temp;
            arg_min_index = index;
        }
    }

    return arg_min_index;
}

double vector_max(const Vector* const vector) {
    double max = -DBL_MIN;

    for (size_t index = 0; index < vector->length; ++index) {
        const double temp = vector_get(vector, index);
        if (temp > max) {
            max = temp;
        }
    }

    return max;
}

size_t vector_argmax(const Vector* const vector) {
    size_t arg_min_index = 0;
    double max = -DBL_MIN;

    for (size_t index = 0; index < vector->length; ++index) {
        const double temp = vector_get(vector, index);
        if (temp > max) {
            max = temp;
            arg_min_index = index;
        }
    }

    return arg_min_index;
}

double vector_sum(const Vector* vector) {
    double sum = 0.0;

    for (size_t index = 0; index < vector->length; ++index) {
        sum += vector_get(vector, index);
    }

    return sum;
}


double vector_mean(const Vector* const vector) {
    double sum = 0.0;

    for (size_t index = 0; index < vector->length; ++index) {
        sum += vector_get(vector, index);
    }

    return sum / vector->length;
}

double vector_variance(const Vector* const vector) {
    double scalar_w_ddof = 1.0 / (vector->length - 1);
    double mean = vector_mean(vector);


    double summation = 0.0;
    for (size_t index = 0; index < vector->length; ++index) {
        summation += pow(vector_get(vector, index) - mean, 2.0);
    }

    return scalar_w_ddof * summation;
}
