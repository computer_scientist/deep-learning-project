#include <torch/torch.h>

torch::TensorOptions get_tensor_device() {
    if (torch::mps::is_available()) {
        std::cout << "Switching to MPS device 🥰" << std::endl;
        return device(torch::kMPS);
    } else if (torch::cuda::is_available()) {
        std::cout << "Switching to CUDA device 🥳" << std::endl;
        return device(torch::kCUDA);
    } else {
        std::cout << "Using CPU ..." << std::endl;
        return device(torch::kCPU);
    }
}

extern "C" {
    double compute_binary_cross_entropy(const double* p, const double* q) {
        const torch::TensorOptions tensor_device = get_tensor_device();

        const torch::Tensor p_tensor = torch::tensor(*p, tensor_device);
        const torch::Tensor q_tensor = torch::tensor(*q, tensor_device);

        const torch::Tensor bce = torch::nn::functional::binary_cross_entropy(q_tensor, p_tensor);

        return bce.mean().item<double>();
    }
}