#include <vector.h>
#include <stdio.h>

void demo_vector_dot_product() {
    const size_t LENGTH = 4;

    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* vector1 = vector_create_malloc(LENGTH);
    Vector* vector2 = vector_create_malloc(LENGTH);

    // Create arrays to be used to build gsl vectors
    const double v1[4] = {1, 2, 3, 4};
    const double v2[4] = {0, 1, 0, -1};

    // Create gsl vectors
    vector_initialize(vector1, v1);
    vector_initialize(vector2, v2);

    // Calculate dot product
    const double result = vector_dot(vector1, vector2);

    // Print results
    printf("Vector Dot Product: %.1f\n", result);

    //=====================
    // FREE RESOURCE
    //=====================
    vector_free(vector1);
    vector_free(vector2);
}
