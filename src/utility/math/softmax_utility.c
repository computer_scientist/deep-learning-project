#include "math_ops.h"
#include "vector.h"
#include "plot.h"

void demo_softmax() {
    const size_t number_elements = 25;
    const int LOW = -5;
    const int HIGH = 15;

    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* value_vector = vector_create_malloc(number_elements);
    Vector* exponential_vector = vector_create_malloc(number_elements);
    Vector* sigma_vector = vector_create_malloc(number_elements);

    vector_initialize_random(value_vector, LOW, HIGH);

    softmax(value_vector, exponential_vector, sigma_vector);

    const double sigma_sum = vector_sum(sigma_vector);
    printf("Sigma Sum: %f", sigma_sum);

    const char* const xlabel = "Original number (z)";
    const char* const ylabel = "Softmaxified sigma";

    // Concatenate c pointer string with double
    const int title_length = snprintf(NULL, 0, "∑σ = %.2f", sigma_sum);
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    char* title = malloc(title_length + 1);
    // snprintf(title, "∑σ = %f", sigma_sum);

    plot(value_vector, sigma_vector, xlabel, ylabel, title);

    //=====================
    // FREE RESOURCE
    //=====================
    free(title);
    vector_free(value_vector);
    vector_free(exponential_vector);
    vector_free(sigma_vector);
}
