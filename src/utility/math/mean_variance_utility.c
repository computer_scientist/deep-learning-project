#include "vector.h"
#include "stdio.h"
#include <pthread.h>

void vector_mean_demo();
void vector_variance_demo();

void* mean_variance_thread_1(void *arg) {
    vector_mean_demo();

    return NULL;
}

void* mean_variance_thread_2(void *arg) {
    vector_variance_demo();

    return NULL;
}

void demo_mean_variance() {
    pthread_t thread1, thread2;

    pthread_create(&thread1, NULL, mean_variance_thread_1, NULL);
    pthread_create(&thread2, NULL, mean_variance_thread_2, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
}

void vector_mean_demo() {
    const size_t LENGTH = 7;
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* vector = vector_create_malloc(LENGTH);
    double array[] = {1,2,4,6,5,4,0};

    vector_initialize(vector, array);
    double mean = vector_mean(vector);

    //=====================
    // FREE RESOURCE
    //=====================
    vector_free(vector);

    printf("Vector Mean: %f\n", mean);
}

void vector_variance_demo() {
    const size_t LENGTH = 7;
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* vector = vector_create_malloc(LENGTH);
    double array[] = {1,2,4,6,5,4,0};

    vector_initialize(vector, array);
    double variance = vector_variance(vector);

    //=====================
    // FREE RESOURCE
    //=====================
    vector_free(vector);

    printf("Vector Variance: %f\n", variance);
}
