#include "math_ops.h"
#include "torch_wrapper.h"
#include <stdio.h>

void demo_entropy() {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* probability_vector = vector_create_malloc(2);

    const double probability_array[] = {0.25, 0.75};
    vector_initialize(probability_vector, probability_array);

    const double entropy1 = entropy(probability_vector);
    printf("Entropy: %f\n", entropy1);

    //=====================
    // FREE RESOURCE
    //=====================
    vector_free(probability_vector);
}

void demo_cross_entropy() {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* p_vector = vector_create_malloc(2);
    Vector* q_vector = vector_create_malloc(2);

    const double p_array[] = {1, 0};
    const double q_array[] = {0.25, 0.75};

    vector_initialize(p_vector, p_array);
    vector_initialize(q_vector, q_array);

    const double cross_entropy1 = cross_entropy(p_vector, q_vector);
    printf("Cross Entropy: %f\n", cross_entropy1);

    //=====================
    // FREE RESOURCE
    //=====================
    vector_free(p_vector);
    vector_free(q_vector);
}

void demo_torch_cross_entropy() {
    const double p[] = {1.0, 0.0};
    const double q[] = {0.25, 0.75};

    const double bce = compute_binary_cross_entropy(p, q);

    printf("BCE result: %f", bce);
}
