#include "matrix.h"
#include "stdlib.h"

void demo_matrix_multiplication() {

    const size_t DIMENSION_1 = 3;
    const size_t DIMENSION_2 = 4;
    const size_t DIMENSION_3 = 5;

    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Matrix* matrix1 = matrix_create_malloc(DIMENSION_1, DIMENSION_2);
    Matrix* matrix2 = matrix_create_malloc(DIMENSION_2, DIMENSION_3);
    Matrix* result_matrix = matrix_create_malloc(DIMENSION_1, DIMENSION_3);

    matrix_random_initialize(matrix1);
    matrix_random_initialize(matrix2);

    printf("Matrix 1: (%lu, %lu)\n", DIMENSION_1, DIMENSION_2);
    matrix_print(matrix1);
    printf("Matrix 2: (%lu, %lu)\n", DIMENSION_2, DIMENSION_3);
    matrix_print(matrix2);

    matrix_multiplication(matrix1, matrix2, result_matrix);

    printf("Results: (%lu, %lu)\n", DIMENSION_1, DIMENSION_3);
    matrix_print(result_matrix);

    //=====================
    // FREE RESOURCE
    //=====================
    matrix_free(matrix1);
    matrix_free(matrix2);
    matrix_free(result_matrix);
}
