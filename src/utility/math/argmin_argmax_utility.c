#include <stdio.h>
#include <pthread.h>

#include "matrix.h"
#include "vector.h"
#include "math_utilities.h"

void* thread_1(void *arg) {
    (void)arg; // Explicitly indicate that arg is unused
    demo_vector_argmin_argmax();
    return NULL;
}

void* thread_2(void *arg) {
    (void)arg;
    demo_matrix_argmin_argmax();
    return NULL;
}

void demo_argmin_argmax() {
    pthread_t thread1, thread2;

    pthread_create(&thread1, NULL, thread_1, NULL);
    pthread_create(&thread2, NULL, thread_2, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
}

void demo_vector_argmin_argmax() {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* vector = vector_create_malloc(4);
    const double value_array[] = { 1,40,2,-3 };

    vector_initialize(vector, value_array);

    const size_t arg_min = vector_argmin(vector);
    const size_t arg_max = vector_argmax(vector);

    //=====================
    // FREE RESOURCE
    //=====================
    vector_free(vector);

    printf("Vector ArgMin: %lu\n", arg_min);
    printf("Vector ArgMax: %lu\n", arg_max);
}

void demo_matrix_argmin_argmax() {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Matrix* matrix = matrix_create_malloc(2, 3);

    const double array[2][3] = {
        {0, 1, 10},
        {20, 8, 5}
    };

    matrix_initialize(matrix, *array);

    size_t arg_min[2];
    size_t arg_max[2];
    matrix_argmin(matrix, arg_min);
    matrix_argmax(matrix, arg_max);

    //=====================
    // FREE RESOURCE
    //=====================
    matrix_free(matrix);

    printf("Matrix ArgMin: [%lu][%lu]\n", arg_min[0], arg_min[1]);
    printf("Matrix ArgMax: [%lu][%lu]\n", arg_max[0], arg_max[1]);
}
