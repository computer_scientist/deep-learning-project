#include "matrix.h"

#define DIMENSION_1_A 1
#define DIMENSION_2_A 4
#define DIMENSION_1_B 2
#define DIMENSION_2_B 4

void demo_transpose_vector() {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Matrix* vector1 = matrix_create_malloc(DIMENSION_1_A, DIMENSION_2_A);
    Matrix* vector1_transpose = matrix_create_malloc(DIMENSION_2_A, DIMENSION_1_A);
    Matrix* transpose_vector1_transpose = matrix_create_malloc(DIMENSION_1_A, DIMENSION_2_A);

    // Create vector
    double v1[DIMENSION_1_A][DIMENSION_2_A] = {{1, 2, 3, 4}};
    matrix_initialize(vector1, *v1);
    // Display results
    printf("Original vector:\n");
    matrix_print(vector1);
    printf("\n");

    // Transpose vector
    printf("Transposed vector:\n");
    matrix_transpose(vector1, vector1_transpose);
    matrix_print(vector1_transpose);
    printf("\n");

    // Transpose the transposed vector
    matrix_transpose(vector1_transpose, transpose_vector1_transpose);
    printf("Transposed-transposed vector:\n");
    matrix_print(transpose_vector1_transpose);
    printf("\n");

    //=====================
    // RESOURCE ALLOCATION
    //=====================
    matrix_free(vector1);
    matrix_free(vector1_transpose);
    matrix_free(transpose_vector1_transpose);
}

void demo_transpose_matrix() {
    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Matrix* matrix = matrix_create_malloc(DIMENSION_1_B, DIMENSION_2_B);
    Matrix* matrix_transposed = matrix_create_malloc(DIMENSION_2_B, DIMENSION_1_B);
    Matrix* transpose_matrix_transpose = matrix_create_malloc(DIMENSION_1_B, DIMENSION_2_B);

    // Create matrix
    const double m[2][4] = {
            {1, 2, 3, 4},
            {5, 6, 7, 8}
    };
    matrix_initialize(matrix, *m);

    // Display results
    printf("Original matrix:\n");
    matrix_print(matrix);
    printf("\n");

    // Transpose vector
    printf("Transposed vector:\n");
    matrix_transpose(matrix, matrix_transposed);
    matrix_print(matrix_transposed);
    printf("\n");

    // Transpose the transposed vector
    matrix_transpose(matrix_transposed, transpose_matrix_transpose);
    printf("Transposed-transposed vector:\n");
    matrix_print(transpose_matrix_transpose);
    printf("\n");

    //=====================
    // FREE RESOURCE
    //=====================
    matrix_free(matrix);
    matrix_free(matrix_transposed);
    matrix_free(transpose_matrix_transpose);
}
