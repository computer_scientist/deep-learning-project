#include <math.h>
#include "vector.h"
#include "plot.h"

void demo_logarithm() {
    const int n = 200;
    const double x_end = 20;

    const double step = x_end / n;

    double value_array[n];
    double log_array[n];

    for (int i = 1; i < n; ++i) {
        const double x = i * step;
        value_array[i] = x;
        log_array[i] = log10(x);
    }

    //=====================
    // RESOURCE ALLOCATION
    //=====================
    Vector* x_vector = vector_create_malloc(n);
    Vector* log_vector = vector_create_malloc(n);

    vector_initialize(x_vector, value_array);
    vector_initialize(log_vector, log_array);

    const char* const xlabel = "x";
    const char* const ylabel = "log(x)";
    const char* const title = "Logarithm Demo";

    plot(x_vector, log_vector, xlabel, ylabel, title);

    //=====================
    // Free Resource
    //=====================
    vector_free(x_vector);
    vector_free(log_vector);
}
