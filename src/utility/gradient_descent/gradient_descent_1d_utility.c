#include "math_ops.h"
#include <stdio.h>


void demo_gradient_descent_1d() {
    const int num = 2001;
    const double start = -2.0;
    const double end = 2.0;

    const double learning_rate = .01;
    const int training_epochs = 100;

    double array[num];
    linespace(array, start, end, num);
    const double random_choice = array[rand() % num];
    double local_min = random_choice;

    for (int i = 0; i < training_epochs; ++i) {
        const double gradient = derivative(3, local_min, 2)
                - derivative(3, local_min, 1)
                + derivative(4, 1, 0);
        local_min -= (learning_rate * gradient);
    }
    printf("Gradient Test local min: %.4f", local_min);
}
