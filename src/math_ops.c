#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "math_ops.h"



/**
 * @brief Computes the softmax function for a given input vector.
 *
 * The softmax function takes a vector of values and transforms them into a probability distribution.
 * The probability of each value is calculated by exponentiating the value and normalizing the results.
 * This function computes the softmax function and stores the results in the provided sigma_vector.
 *
 * @param value_vector The input vector.
 * @param exponential_vector The vector to store the exponentiated values.
 * @param sigma_vector The vector to store the softmax values.
 */
void softmax(const Vector* const value_vector, Vector* exponential_vector, Vector* sigma_vector) {
    double sum = 0.0;

    for (size_t i = 0; i < value_vector->length; ++i) {
        const double exp_value = exp(vector_get(value_vector, i));
        vector_set(exponential_vector, i, exp_value);
        sum += exp_value;
    }

    for (size_t i = 0; i < exponential_vector->length; ++i) {
        vector_set(sigma_vector, i, vector_get(exponential_vector, i) / sum);
    }
}

double entropy(const Vector* const probability_vector) {
    double entropy = 0.0;

    // Using ∑-( p*ln(p) )
    // Note: log in c is actually natural log or (ln)
    for (size_t i = 0; i < probability_vector->length; ++i) {
        const double probability = vector_get(probability_vector, i);
        entropy += -(probability * log(probability));
    }

    return entropy;
}

double cross_entropy(const Vector* const p_vector, const Vector* const q_vector) {
    double cross_entropy = 0.0;

    for (size_t i = 0; i < p_vector->length; ++i) {
        const double p = vector_get(p_vector, i);
        const double q = vector_get(q_vector, i);
        cross_entropy += -(p * log(q));
    }

    return cross_entropy;
}

double derivative(const double scalar, const double variable, const double exponent) {
    if (exponent == 0) {
        return 0.0;
    }

    double new_scalar = scalar * exponent;
    double new_exponent = exponent - 1;

    return new_scalar * pow(variable, new_exponent);
}

void linespace(double* array, const double start, const double end, const int num) {
    double step = (end - start) / (num - 1);

    for (size_t index = 0; index < (size_t)num; ++index) {
        array[index] = start + (double)index * step;
    }
}
